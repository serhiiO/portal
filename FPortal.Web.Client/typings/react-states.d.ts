﻿declare namespace AppStates {
    namespace Pages {
        interface IDefaultLayoutState {
            collapsed: boolean;
        }

        interface IInternationalState {
            competition?: FPortal.CompetitionModel;
            activeTab?: string;
        }

        interface TeamProps {
            team?: FPortal.Model.Entities.FootballDataApi.TeamApiModel;
            teamPlayers?: FPortal.Model.Entities.FootballDataApi.TeamPlayersApiModel;
            teamMatches?: FPortal.Model.Entities.FootballDataApi.TeamMatchesCollection;
        }
    }
    namespace Components {
        interface ILeagueTableState {
            leagueTable?: FPortal.Model.Entities.FootballDataApi.LeagueTableApiModel;
        }

        interface GroupStandingsProps {
            groups?: FPortal.Model.Entities.FootballDataApi.GroupTableApiModel;
            teams?: FPortal.Model.Entities.FootballDataApi.SeasonTeamsApiModel;
        }

        interface SeasonMatchesProps {
            matches?: FPortal.Model.Entities.FootballDataApi.SeasonMatchesApiModel;
            teams?: FPortal.Model.Entities.FootballDataApi.SeasonTeamsApiModel;
        }
    }
}