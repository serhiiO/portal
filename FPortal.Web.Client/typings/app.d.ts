﻿/// <reference path="index.d.ts" />

interface Window {
    /**
     * Redux DevTools chrome extension
    **/
    devToolsExtension?: Function;
    React: any;
}

declare module Redux {
    export interface IAction {
        type: string;
        payload?: any
    }
}

declare module FPortal {
    interface AppState {
        userName: string;
        loadbarState: number;
    }

    interface CompetitionModel {
        key: string,
        name: string,
        leagues: Array<number>,
        international: boolean
    }

    interface SuiteApiClient {
        fetchSeasonGroupsTable: (seasonId: number, matchday?: number) => Promise<FPortal.Model.Entities.FootballDataApi.GroupTableApiModel>;
        fetchSeasonLeagueTable: (seasonId: number, matchday?: number) => Promise<FPortal.Model.Entities.FootballDataApi.LeagueTableApiModel>;
        fetchSeasonMatches: (seasonId: number, matchday?: number, timeFrame?: string) => Promise<FPortal.Model.Entities.FootballDataApi.SeasonMatchesApiModel>;
        fetchSeasonTeams: (seasonId: number) => Promise<FPortal.Model.Entities.FootballDataApi.SeasonTeamsApiModel>;

        fetchTeam: (teamId: number) => Promise<FPortal.Model.Entities.FootballDataApi.TeamApiModel>;
        fetchTeamMatches: (teamId: number, year?: number, timeFrame?: string, venue?: FPortal.Model.Entities.FootballDataApi.MatchVenue) => Promise<FPortal.Model.Entities.FootballDataApi.TeamMatchesCollection>;
        fetchTeamPlayers: (teamId: number) => Promise<FPortal.Model.Entities.FootballDataApi.TeamPlayersApiModel>;
    }
}



declare namespace ReduxThunk {
    export interface ThunkInterfaceEx<TResult> {
        <T>(dispatch: Redux.Dispatch, getState?: () => T): TResult;
    }
}

interface Promise<T> extends Promise.IThenable<T> { }