﻿
 
 

 

/// <reference path="Enums.generated.ts" />

declare module FPortal.Model.Entities.FootballDataApi {
	interface BaseApiModel<TLinkCollection> {
		_links: TLinkCollection;
	}
	interface FootballSeasonApiModel extends FPortal.Model.Entities.FootballDataApi.BaseApiModel<FPortal.Model.Entities.FootballDataApi.LinkModels.FootballSeasonLinkCollection> {
		caption: string;
		currentMatchday: number;
		id: number;
		lastUpdated: string;
		league: FPortal.Model.Entities.FootballDataApi.LeagueCode;
		numberOfGames: number;
		numberOfMatchdays: number;
		numberOfTeams: number;
		year: number;
	}
	interface GroupTableApiModel {
		leagueCaption: string;
		matchday: number;
		standings: {[key: string]:  FPortal.Model.Entities.FootballDataApi.GroupTeamStandingApiModel[]};
	}
	interface GroupTeamStandingApiModel {
		crestURI: string;
		goalDifference: number;
		goals: number;
		goalsAgainst: number;
		group: string;
		playedGames: number;
		points: number;
		rank: number;
		team: string;
		teamId: number;
	}
	interface HeadToHead extends FPortal.Model.Entities.FootballDataApi.MatchDetailsCollection {
		awayTeamWins: number;
		homeTeamWins: number;
		lastWinAwayTeam: FPortal.Model.Entities.FootballDataApi.MatchDetailsApiModel;
		lastWinHomeTeam: FPortal.Model.Entities.FootballDataApi.MatchDetailsApiModel;
	}
	interface LeagueTableApiModel extends FPortal.Model.Entities.FootballDataApi.BaseApiModel<FPortal.Model.Entities.FootballDataApi.LinkModels.LeagueTableLinkCollection> {
		leagueCaption: string;
		matchday: number;
		standing: FPortal.Model.Entities.FootballDataApi.TeamStandingApiModel[];
	}
	interface Match {
		headToHead: FPortal.Model.Entities.FootballDataApi.HeadToHead;
		matchDetails: FPortal.Model.Entities.FootballDataApi.MatchDetailsApiModel;
	}
	interface MatchDetailsApiModel extends FPortal.Model.Entities.FootballDataApi.BaseApiModel<FPortal.Model.Entities.FootballDataApi.LinkModels.MatchDetailsLinkCollection> {
		awayTeamName: string;
		date: string;
		homeTeamName: string;
		matchday: number;
		result: FPortal.Model.Entities.FootballDataApi.MatchResult;
		status: FPortal.Model.Entities.FootballDataApi.MatchStatus;
	}
	interface MatchDetailsCollection {
		count: number;
		matchesDetails: FPortal.Model.Entities.FootballDataApi.MatchDetailsApiModel[];
		timeFrameEnd: string;
		timeFrameStart: string;
	}
	interface MatchResult {
		goalsAwayTeam: number;
		goalsHomeTeam: number;
	}
	interface SeasonMatchesApiModel extends FPortal.Model.Entities.FootballDataApi.BaseApiModel<FPortal.Model.Entities.FootballDataApi.LinkModels.SeasonMatchesLinkCollection> {
		count: number;
		fixtures: FPortal.Model.Entities.FootballDataApi.MatchDetailsApiModel[];
	}
	interface SeasonTeamsApiModel extends FPortal.Model.Entities.FootballDataApi.BaseApiModel<FPortal.Model.Entities.FootballDataApi.LinkModels.SeasonTeamsLinkCollection> {
		count: number;
		teams: FPortal.Model.Entities.FootballDataApi.TeamApiModel[];
	}
	interface TeamApiModel extends FPortal.Model.Entities.FootballDataApi.BaseApiModel<FPortal.Model.Entities.FootballDataApi.LinkModels.TeamLinkCollection> {
		code: string;
		crestUrl: string;
		name: string;
		shortName: string;
		squadMarketValue: string;
	}
	interface TeamMatchesCollection extends FPortal.Model.Entities.FootballDataApi.BaseApiModel<FPortal.Model.Entities.FootballDataApi.LinkModels.TeamMatchesLinkCollection> {
		count: number;
		matchesDetails: FPortal.Model.Entities.FootballDataApi.MatchDetailsApiModel[];
		timeFrameEnd: string;
		timeFrameStart: string;
	}
	interface TeamPlayer {
		contractUntil: string;
		dateOfBirth: string;
		jerseyNumber: string;
		marketValue: string;
		name: string;
		nationality: string;
		position: string;
	}
	interface TeamPlayersApiModel extends FPortal.Model.Entities.FootballDataApi.BaseApiModel<FPortal.Model.Entities.FootballDataApi.LinkModels.TeamPlayersLinkCollection> {
		count: number;
		players: FPortal.Model.Entities.FootballDataApi.TeamPlayer[];
	}
	interface TeamPlayStatistic {
		draws: number;
		goals: number;
		goalsAgainst: number;
		losses: number;
		wins: number;
	}
	interface TeamStandingApiModel extends FPortal.Model.Entities.FootballDataApi.BaseApiModel<FPortal.Model.Entities.FootballDataApi.LinkModels.TeamStandingLinkCollection> {
		away: FPortal.Model.Entities.FootballDataApi.TeamPlayStatistic;
		crestURI: string;
		draws: number;
		goalDifference: number;
		goals: number;
		goalsAgainst: number;
		home: FPortal.Model.Entities.FootballDataApi.TeamPlayStatistic;
		losses: number;
		playedGames: number;
		points: number;
		position: number;
		teamName: string;
		wins: number;
	}
	interface TimeFrame {
	}
}
declare module FPortal.Model.Entities.FootballDataApi.LinkModels {
	interface FootballSeasonLinkCollection extends FPortal.Model.Entities.FootballDataApi.LinkModels.LinkCollection {
		fixtures: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
		leagueTable: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
		teams: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
	}
	interface LeagueTableLinkCollection extends FPortal.Model.Entities.FootballDataApi.LinkModels.LinkCollection {
		soccerseason: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
	}
	interface Link {
		href: string;
	}
	interface LinkCollection {
		self: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
	}
	interface MatchDetailsLinkCollection extends FPortal.Model.Entities.FootballDataApi.LinkModels.LinkCollection {
		awayTeam: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
		homeTeam: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
		soccerseason: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
	}
	interface SeasonMatchesLinkCollection extends FPortal.Model.Entities.FootballDataApi.LinkModels.LinkCollection {
		soccerseason: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
	}
	interface SeasonTeamsLinkCollection extends FPortal.Model.Entities.FootballDataApi.LinkModels.LinkCollection {
		soccerseason: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
	}
	interface TeamLinkCollection extends FPortal.Model.Entities.FootballDataApi.LinkModels.LinkCollection {
		fixtures: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
		players: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
	}
	interface TeamMatchesLinkCollection extends FPortal.Model.Entities.FootballDataApi.LinkModels.LinkCollection {
		team: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
	}
	interface TeamPlayersLinkCollection extends FPortal.Model.Entities.FootballDataApi.LinkModels.LinkCollection {
		team: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
	}
	interface TeamStandingLinkCollection {
		team: FPortal.Model.Entities.FootballDataApi.LinkModels.Link;
	}
}
declare module System.Collections.Generic {
	interface KeyValuePair<TKey, TValue> {
		key: TKey;
		value: TValue;
	}
}


