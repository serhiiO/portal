﻿declare namespace AppProps {
    interface SuiteApiConnectedProps {
        api?: FPortal.SuiteApiClient;
    }

    namespace Pages {

        interface LeageKeyParams {
            key: string
        }

        interface DomesticProps extends ReactRouter.RouteComponentProps<LeageKeyParams, any> {
        }

        interface InternationalOwnProps extends ReactRouter.RouteComponentProps<LeageKeyParams, any> {
        }

        interface InternationalConnectedProps extends SuiteApiConnectedProps { }

        interface InternationalProps extends InternationalOwnProps, InternationalConnectedProps { }

        interface TeamParams {
            id: number;
        }

        interface TeamOwnProps extends ReactRouter.RouteComponentProps<TeamParams, any> {
        }

        interface TeamConnectedProps extends SuiteApiConnectedProps { }

        interface TeamProps extends TeamOwnProps, TeamConnectedProps { }
    }

    namespace Components {
        interface LoadbarProps {
            loading?: number;
        }

        interface NavbarProps {
            onMenuToggled: (newState: boolean) => void;
            initialMenuState: boolean;
        }

        interface LeagueTableConnectedProps extends SuiteApiConnectedProps { }

        interface LeagueTableOwnProps {
            leagueKey: number;
        }

        interface LeagueTableProps extends LeagueTableOwnProps, LeagueTableConnectedProps { }

        interface GroupStandingsOwnProps {
            seasonKey: number;
        }

        interface GroupStandingsConnectedProps extends SuiteApiConnectedProps { }

        interface GroupStandingsProps extends GroupStandingsOwnProps, GroupStandingsConnectedProps { }

        interface SeasonMatchesOwnProps {
            seasonKey: number;
        }

        interface SeasonMatchesConnectedProps extends SuiteApiConnectedProps { }

        interface SeasonMatchesProps extends SeasonMatchesOwnProps, SeasonMatchesConnectedProps { }

        interface GroupTableProps {
            teams: FPortal.Model.Entities.FootballDataApi.GroupTeamStandingApiModel[];
        }
    }
}