module FPortal.Model.Entities.Common {
	export enum Countries {
		None = 0,
		Ukraine = 1,
		Russia = 2,
		England = 3,
		Spain = 4,
		Germany = 5,
		Italy = 6,
		France = 7,
		Netherlands = 8,
		Portugal = 9
	}
}
module FPortal.Model.Entities.FootballDataApi {
	export enum ApiVersion {
		v1 = 0,
		alpha = 1
	}
	export enum LeagueCode {
		EMPTY = 0,
		BL1 = 1,
		BL2 = 2,
		BL3 = 3,
		FL1 = 4,
		FL2 = 5,
		PL = 6,
		PD = 7,
		SD = 8,
		SA = 9,
		PPL = 10,
		DED = 11,
		CL = 12,
		EL1 = 13,
		EU16 = 14
	}
	export enum MatchStatus {
		NONE = 0,
		TIMED = 1,
		IN_PLAY = 2,
		FINISHED = 3,
		SCHEDULED = 4
	}
	export enum MatchVenue {
		unset = 0,
		home = 1,
		away = 2
	}
}
module System {
	export enum UriHostNameType {
		Unknown = 0,
		Basic = 1,
		Dns = 2,
		IPv4 = 3,
		IPv6 = 4
	}
}

