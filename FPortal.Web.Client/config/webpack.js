'use strict';

var webpack = require('webpack');
var donePlugin = function () {
    this.plugin("done", function (stats) {
        if (stats.compilation.errors && stats.compilation.errors.length && process.argv.indexOf('--watch') == -1) {
            console.log(stats.compilation.errors);
            process.exit(1);
            //throw new Error(stats.compilation.errors);
        }
        // ...
    });
};

module.exports = function (release, path, publicPath) {
    var plugins = [
        donePlugin,
        new webpack.ProvidePlugin({
            // whatwg fetch polyfill (requires imports-loader and exports-loader npm modules)
            "fetch": "imports?this=>global!exports?global.fetch!whatwg-fetch",
            "FPortal": "imports?this=>global!exports?FPortal!generated-enums",
            "_": "lodash"
        })
    ];

    if (release) {
        plugins.push(new webpack.DefinePlugin({ 'process.env.NODE_ENV': '"production"' }));
        plugins.push(new webpack.optimize.DedupePlugin());
        plugins.push(new webpack.optimize.UglifyJsPlugin());
        plugins.push(new webpack.optimize.OccurenceOrderPlugin());
        plugins.push(new webpack.optimize.AggressiveMergingPlugin());
    }

    return {
        output: {
            path: path,
            filename: 'app.js',
            publicPatch: publicPath
        },

        profile: true,
        cache: !release,
        debug: !release,
        devtool: release ? false : "sourcemap",
        entry: {
            app: './src/App.tsx'
        },

        stats: {
            colors: true,
            reasons: true
        },

        plugins: plugins,

        resolve: {
            alias: {
                "generated-enums": __dirname + "/../typings/Enums.generated.ts"
            },
            extensions: ['', '.webpack.js', '.web.js', '.js', 'ts', 'tsx']
        },

        module: {
            loaders: [
              {
                  test: /\.css$/,
                  loader: 'style!css'
              },
              {
                  test: /\.less$/,
                  loader: 'style!css!less'
              },
              {
                  test: /\.gif/,
                  loader: 'url-loader?limit=10000&mimetype=image/gif'
              },
              {
                  test: /\.jpg/,
                  loader: 'url-loader?limit=10000&mimetype=image/jpg'
              },
              {
                  test: /\.png/,
                  loader: 'url-loader?limit=10000&mimetype=image/png'
              },
              {
                  test: /\.tsx?$/,
                  loader: 'ts-loader'
              }
            ]
        }
    };
};
