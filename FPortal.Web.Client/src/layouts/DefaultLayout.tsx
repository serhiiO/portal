﻿import * as React from "react";
import { Link } from "react-router";
import Navbar from "../components/Navbar.tsx";
import Footer from "../components/Footer.tsx";
import Loadbar from "../components/Loadbar.tsx";

export default class DefaultLayout extends React.Component<any, AppStates.Pages.IDefaultLayoutState> {
    constructor(props, context) {
        super(props, context);
        this.state = {
            collapsed: true
        }
    }

    toggleNavbar = (newState: boolean) => {
        this.setState({ collapsed: newState });
    }

    render() {
        let containerClasses = "flex-layout";
        if (this.state.collapsed) containerClasses += " aside-collapsed";

        return (
            <div className={containerClasses}>
                <Navbar onMenuToggled={this.toggleNavbar} initialMenuState={this.state.collapsed} />
                <Loadbar />
                <section className="container flex-container">
                    {this.props.children}
                </section>
                <Footer />
            </div>
        );
    }
}