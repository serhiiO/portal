﻿import * as React from "react";
import { connect } from "react-redux";
import * as NProgress from "nprogress";

function mapStateToProps(state: FPortal.AppState, ownProps?: any): AppProps.Components.LoadbarProps {
    return {
        loading: state.loadbarState
    };
}

var loadbar = class Loadbar extends React.Component<AppProps.Components.LoadbarProps, {}> {
    constructor(props, context) {
        super(props, context);
    }

    componentWillReceiveProps(nextProps: AppProps.Components.LoadbarProps, nextContext: any): void {
        if (nextProps.loading > 0 && this.props.loading == 0) {
            NProgress.start();
        }
        else if (nextProps.loading > 0 && nextProps.loading < this.props.loading) {
            NProgress.inc();
        }
        else if (this.props.loading > 0 && nextProps.loading == 0) {
            NProgress.done();
        }
    }

    render() {
        return null;
    }
}

export default connect(mapStateToProps)(loadbar);