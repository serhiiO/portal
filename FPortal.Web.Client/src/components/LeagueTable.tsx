﻿import * as React from "react";
import { connect } from "react-redux";
import createSuiteApi from "../businessLogic/suiteApiClient.ts";

function mapActionsToPops(dispatch: Redux.Dispatch, ownProps?: AppProps.Components.LeagueTableOwnProps): AppProps.Components.LeagueTableConnectedProps {
    return {
        api: createSuiteApi(dispatch)
    };
}

const leagueTable = class LeagueTable extends React.Component<AppProps.Components.LeagueTableProps, AppStates.Components.ILeagueTableState> {
    private maxMatchday: number;

    constructor(props: AppProps.Components.LeagueTableProps, context) {
        super(props, context);
        this.maxMatchday = -1;

        this.state = {
            leagueTable: null
        }
    }

    private createLeageTableRow(standing: FPortal.Model.Entities.FootballDataApi.TeamStandingApiModel[]): JSX.Element[] {
        return standing.map((element, index, array) => {
            return (
                <tr key={element.teamName} >
                    <td>{element.position}</td>
                    <td><img src={element.crestURI} width="14px" height="14px" /></td>
                    <td>{element.teamName}</td>
                    <td>{element.playedGames}</td>
                    <td>{element.wins}</td>
                    <td>{element.draws}</td>
                    <td>{element.losses}</td>
                    <td>{element.goals}</td>
                    <td>{element.goalsAgainst}</td>
                    <td>{element.goalDifference}</td>
                    <td>{element.points}</td>
                </tr>
            );
        })
    }

    private loadTable(seasonId: number, matchday?: number) {
        this.props.api.fetchSeasonLeagueTable(seasonId, matchday)
            .then(data => {
                if (this.maxMatchday == -1) this.maxMatchday = data.matchday;
                this.setState({ leagueTable: data });
            });
    }

    private changeMatchday = (matchday: number) => {
        if (matchday <= 1 || matchday > this.maxMatchday)
            return;

        this.loadTable(this.props.leagueKey, matchday);
    }

    componentWillReceiveProps(nextProps: AppProps.Components.LeagueTableProps, nextContext: any): void {
        if (nextProps.leagueKey != this.props.leagueKey) {
            this.maxMatchday = -1;
            this.loadTable(nextProps.leagueKey);
        }
    }

    componentDidMount(): void {
        this.loadTable(this.props.leagueKey);
    }

    render() {
        if (!this.state.leagueTable)
            return null;

        const rows = this.createLeageTableRow(this.state.leagueTable.standing);
        let leftArrowClasses = "icon-circle-left matchday-control";
        if (this.state.leagueTable.matchday <= 1)
            leftArrowClasses += " disabled";
        let rightArrowClasses = "icon-circle-right matchday-control";
        if (this.state.leagueTable.matchday >= this.maxMatchday || this.maxMatchday < 0)
            rightArrowClasses += " disabled";

        return (
            <div>
                <div>
                    <span className={leftArrowClasses} onClick={() => this.changeMatchday(this.state.leagueTable.matchday - 1) }></span>
                    <h3 className="matchday-headline">Matchday {this.state.leagueTable.matchday}</h3>
                    <span className={rightArrowClasses} onClick={() => this.changeMatchday(this.state.leagueTable.matchday + 1) }></span>
                </div>
                <table>
                    <thead>
                        <tr>
                            <td>#</td>
                            <td></td>
                            <td>Team</td>
                            <td>P</td>
                            <td>W</td>
                            <td>D</td>
                            <td>L</td>
                            <td>G</td>
                            <td>GA</td>
                            <td>GD</td>
                            <td>Points</td>
                        </tr>
                    </thead>
                    <tbody>
                        {rows}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default connect(null, mapActionsToPops)(leagueTable);