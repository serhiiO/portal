import * as React from "react";
import { Link } from "react-router";
import competitionsLeagueMap from "../businessLogic/competitionsLeagueMap.ts";

export default class Navbar extends React.Component<AppProps.Components.NavbarProps, any> {
    private menuState: boolean;

    constructor(props: AppProps.Components.NavbarProps, context) {
        super(props, context);

        this.menuState = props.initialMenuState;
    }

    private toggleCollapsedMode = (e: React.MouseEvent) => {
        this.menuState = !this.menuState;
        this.props.onMenuToggled(this.menuState);
    }

    private createNavbarElement(competition: FPortal.CompetitionModel): JSX.Element {
        return (
            <li key={competition.key}>
                <Link activeClassName="active" to={`/overview/${competition.key}`}>
                    <span className={`svgicon-flag-${competition.key}`}></span>
                    <span className="sidebar-link-text">{competition.name}</span>
                </Link>
            </li>
        );
    }

    render() {
        const navbarLinks = competitionsLeagueMap.map((competition) => {
            return this.createNavbarElement(competition);
        });

        return (
            <div>
                <header className="topnavbar-wrapper">
                    <nav className="navbar topnavbar">
                        <div className="navbar-header">
                            <Link to="/" className="navbar-brand">
                                <div className="brand-logo">
                                    <em className="icon-logo"></em>
                                    <span>Football Portal</span>
                                </div>
                            </Link>
                        </div>
                        <div className="topnavbar-inner">
                            <ul className="nav-list-left">
                                <li onClick={this.toggleCollapsedMode} >
                                    <span className="icon-menu"></span>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>
                <aside className="aside-navbar-wrapper">
                    <div className="aside-inner">
                        <nav className="sidebar">
                            <ul className="nav">
                                {navbarLinks}
                            </ul>
                        </nav>
                    </div>
                </aside>
            </div>
        );
    }
}