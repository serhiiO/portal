﻿import * as React from "react";
import { Link } from "react-router";

export default class Footer extends React.Component<any, any> {
    render() {
        return (
            <footer>
                <span>© {new Date().getFullYear() } - Football Portal</span>
                <ul className="socials">
                    <li>
                        <a className="icon-facebook" target="_blank" href="https://www.facebook.com/" />
                    </li>
                    <li>
                        <a className="icon-vk" target="_blank" href="https://vk.com/" />
                    </li>
                </ul>
            </footer>
        );
    }
}