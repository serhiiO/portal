﻿import * as React from "react";
import { connect } from "react-redux";
import createSuiteApi from "../businessLogic/suiteApiClient.ts";
import * as dateformat from "dateformat";


function mapActionsToPops(dispatch: Redux.Dispatch, ownProps?: AppProps.Components.SeasonMatchesOwnProps): AppProps.Components.SeasonMatchesConnectedProps {
    return {
        api: createSuiteApi(dispatch)
    };
}

class SeasonMatches extends React.Component<AppProps.Components.SeasonMatchesProps, AppStates.Components.SeasonMatchesProps> {
    constructor(props: AppProps.Components.SeasonMatchesProps, context: any) {
        super(props, context);
        this.state = {
            matches: null
        };
    }

    render() {
        if (!this.state.matches) return null;
        
        return (
            <div className="season-matches">
                { this.createDateGroups(this.state.matches.fixtures) }
            </div>
        );
    }

    // React Element builders
    createDateGroups = (matches: FPortal.Model.Entities.FootballDataApi.MatchDetailsApiModel[]): JSX.Element[] => {
        let groups: JSX.Element[] = [];

        var dateGroups = _.groupBy(matches, (element) => {
            return new Date(element.date).toDateString();
        });
        for (let date in dateGroups) {
            let group = (
                <div className="date-group-wrap" key={date} >
                    <div className="date-group">
                        <h3>{ dateformat(date, "dd-mm-yyyy") }</h3>
                        <div className="table-wrap">
                            { this.createMatchesTable(dateGroups[date]) }
                        </div>
                    </div>
                </div>
            );
            groups.push(group);
        }
        return groups;
    }

    createMatchesTable = (matches: FPortal.Model.Entities.FootballDataApi.MatchDetailsApiModel[]): JSX.Element => {
        var matchesElements = matches.map(this.createMatchRow);

        return (
            <table>
                <tbody>
                    {matchesElements}
                </tbody>
            </table>
        );
    }

    createMatchRow = (match: FPortal.Model.Entities.FootballDataApi.MatchDetailsApiModel): JSX.Element => {
        let homeTeamScoreElement = <td className="score home">-</td>;
        let awayTeamScoreElement = <td className="score away">-</td>;

        switch (match.status) {
            case FPortal.Model.Entities.FootballDataApi.MatchStatus.FINISHED:
                homeTeamScoreElement = <td className="score home">{match.result.goalsHomeTeam}</td>;
                awayTeamScoreElement = <td className="score away">{match.result.goalsAwayTeam}</td>;
                break;
            case FPortal.Model.Entities.FootballDataApi.MatchStatus.IN_PLAY:
                homeTeamScoreElement = <td className="score home in-progress">{match.result.goalsHomeTeam}</td>;
                awayTeamScoreElement = <td className="score away in-progress">{match.result.goalsAwayTeam}</td>;
                break;
        }
        
        return (
            <tr key={match.date + match.homeTeamName + match.awayTeamName}>
                <td>{ dateformat(match.date, "HH:MM") }</td>
                <td className="team-name home">{match.homeTeamName}</td>
                <td className="short-team-name home">{ this.getShortTeamName(match.homeTeamName) }</td>
                <td className="team-badge"><img src={ this.getCrestUrl(match.homeTeamName) } width="20px" height="20px" /></td>
                {homeTeamScoreElement}
                <td>: </td>
                {awayTeamScoreElement}
                <td className="team-badge"><img src={ this.getCrestUrl(match.awayTeamName) } width="20px" height="20px" /></td>
                <td className="short-team-name away">{ this.getShortTeamName(match.awayTeamName) }</td>
                <td className="team-name away">{match.awayTeamName}</td>
                <td className="more-button"><a>more...</a></td>
            </tr>
        );
    }

    // React Lifecycle Methods
    componentDidMount(): void {
        this.loadSeasonMatches(this.props.seasonKey);
    }

    componentWillReceiveProps(nextProps: AppProps.Components.SeasonMatchesProps, nextContext: any): void {
        if (nextProps.seasonKey != this.props.seasonKey) {
            this.loadSeasonMatches(nextProps.seasonKey);
        }
    }

    // Logic
    loadSeasonMatches = (seasonId: number, matchday: number = null, timeFrame: string = null) => {
        this.props.api.fetchSeasonMatches(seasonId, matchday, timeFrame)
            .then(value => this.setState({ matches: value }))
            .catch(error => this.setState({ matches: null }));

        // load team for retrieving badges
        this.props.api.fetchSeasonTeams(seasonId)
            .then(value => this.setState({ teams: value }))
            .catch(error => this.setState({ teams: null }));
    }

    getCrestUrl = (teamName: string): string => {
        let result = "";
        if (this.state.teams) {
            const team = _.find(this.state.teams.teams, team => team.name == teamName);
            if (team) result = team.crestUrl;
        }

        return result;
    }

    getShortTeamName = (teamName: string): string => {
        let result = teamName;
        if (this.state.teams) {
            const team = _.find(this.state.teams.teams, team => team.name == teamName);
            if (team) result = team.shortName || team.code;
        }

        return result;
    }
}

export default connect(null, mapActionsToPops)(SeasonMatches);