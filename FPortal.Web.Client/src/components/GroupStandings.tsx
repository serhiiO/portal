﻿import * as React from "react";
import { connect } from "react-redux";
import { Link } from "react-router";
import createSuiteApi from "../businessLogic/suiteApiClient.ts";

function mapActionsToPops(dispatch: Redux.Dispatch, ownProps?: AppProps.Components.GroupStandingsOwnProps): AppProps.Components.GroupStandingsConnectedProps {
    return {
        api: createSuiteApi(dispatch)
    };
}

class GroupStandings extends React.Component<AppProps.Components.GroupStandingsProps, AppStates.Components.GroupStandingsProps> {
    constructor(props: AppProps.Components.GroupStandingsProps, context: any) {
        super(props, context);
        this.state = {
            groups: null
        };
    }

    render() {
        if (!this.state.groups) return null;

        return (
            <div className="groups-overview">
                { this.createGroups() }
            </div>
        );
    }

    // React Element builders
    createGroups(): JSX.Element[] {
        let groups: JSX.Element[] = [];
        for (let groupName in this.state.groups.standings) {
            let group = (
                <div className="flex-group" key={groupName} >
                    <div className="group-wrap">
                        <h3>Group {groupName.toUpperCase() }</h3>
                        <div className="table-wrap">
                            <table>
                                { this.createTableHeader() }
                                { this.createTableBody(this.state.groups.standings[groupName]) }
                            </table>
                        </div>
                    </div>
                </div>
            );
            groups.push(group);
        }
        return groups;
    }

    private createTableHeader(): JSX.Element {
        return (
            <thead>
                <tr>
                    <td className="position">#</td>
                    <td></td>
                    <td className="team">Team</td>
                    <td className="short-team">Team</td>
                    <td className="stats">P</td>
                    <td className="stats">G</td>
                    <td className="stats">GA</td>
                    <td className="stats">GD</td>
                    <td className="stats">PT</td>
                </tr>
            </thead>
        );
    }

    private createTableBody(standing: FPortal.Model.Entities.FootballDataApi.GroupTeamStandingApiModel[]): JSX.Element {
        var rows = standing.map((element, index, array) => {
            return (
                <tr key={element.team} >
                    <td className="position">{element.rank}</td>
                    <td><img src={element.crestURI} width="20px" height="20px" /></td>
                    <td className="team"><Link to={"/teams/" + element.teamId}>{element.team}</Link></td>
                    <td className="short-team">{ this.getShortTeamName(element.team) }</td>
                    <td className="stats">{element.playedGames}</td>
                    <td className="stats">{element.goals}</td>
                    <td className="stats">{element.goalsAgainst}</td>
                    <td className="stats">{element.goalDifference}</td>
                    <td className="stats">{element.points}</td>
                </tr>
            );
        })

        return <tbody>{rows}</tbody>;
    }

    // React Lifecycle Methods
    componentDidMount(): void {
        this.loadSeasonGroups(this.props.seasonKey);
        this.loadSeasonTeams(this.props.seasonKey);
    }

    componentWillReceiveProps(nextProps: AppProps.Components.GroupStandingsProps, nextContext: any): void {
        if (nextProps.seasonKey != this.props.seasonKey) {
            this.loadSeasonGroups(nextProps.seasonKey);
            this.loadSeasonTeams(nextProps.seasonKey);
        }
    }

    // Logic
    loadSeasonGroups(seasonId: number, matchday: number = null) {
        this.props.api.fetchSeasonGroupsTable(seasonId, matchday)
            .then(value => this.setState({ groups: value }))
            .catch(error => this.setState({ groups: null }))
    }

    loadSeasonTeams(seasonId: number) {
        this.props.api.fetchSeasonTeams(seasonId)
            .then(value => this.setState({ teams: value }))
            .catch(error => this.setState({ teams: null }))
    }

    getShortTeamName = (teamName: string): string => {
        let result = teamName;
        if (this.state.teams) {
            const team = _.find(this.state.teams.teams, team => team.name == teamName);
            if (team) result = team.shortName || team.code;
        }

        return result;
    }
}

export default connect(null, mapActionsToPops)(GroupStandings);