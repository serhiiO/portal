﻿import { LOGIN_USER } from "../actions/actionTypes.ts";

export default function userReducer(state = null, action: Redux.IAction) {
    if (action.type == LOGIN_USER) {
        return action.payload;
    }
    return state;
}