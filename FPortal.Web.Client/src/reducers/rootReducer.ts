﻿import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";

import userReducer from "./userReducer.ts";
import loadbarReducer from "./loadbarReducer.ts";



const rootReducer = combineReducers({
    routing: routerReducer,
    userName: userReducer,
    loadbarState: loadbarReducer,
});

export default rootReducer;