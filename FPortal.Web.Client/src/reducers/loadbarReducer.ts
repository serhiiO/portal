﻿import { INCREMENT_LOADINGBAR, DECREMENT_LOADINGBAR } from "../middlewares/loadbarMiddleware";

export default function loadbarReducer(state = 0, action: Redux.IAction) {
    if (action.type == INCREMENT_LOADINGBAR) {
        state++;
    }
    if (action.type == DECREMENT_LOADINGBAR) {
        state == 0 ? state : state--;
    }

    return state;
}