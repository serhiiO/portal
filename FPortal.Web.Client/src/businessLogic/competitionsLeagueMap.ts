﻿const competitionsLeagueMap: FPortal.CompetitionModel[] = [
    {
        key: "euro-2016",
        name: "UEFA EURO 2016",
        leagues: [424],
        international: true
    },
    {
        key: "champions-league",
        name: "Champions League",
        leagues: [405],
        international: true
    },
    {
        key: "england",
        name: "England",
        leagues: [398, 425],
        international: false
    },
    {
        key: "spain",
        name: "Spain",
        leagues: [399, 400],
        international: false
    },
    {
        key: "germany",
        name: "Germany",
        leagues: [394, 395, 403],
        international: false
    },
    {
        key: "italy",
        name: "Italy",
        leagues: [401],
        international: false
    },
    {
        key: "france",
        name: "France",
        leagues: [396, 397],
        international: false
    },
    {
        key: "netherlands",
        name: "Netherlands",
        leagues: [404],
        international: false
    },
    {
        key: "portugal",
        name: "Portugal",
        leagues: [402],
        international: false
    }
];

export default competitionsLeagueMap;