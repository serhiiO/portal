﻿import * as seasonApi from "./suiteApi/season.ts";
import * as teamApi from "./suiteApi/team.ts";

export default function createSuiteApi(dispatch: Redux.Dispatch): FPortal.SuiteApiClient {
    return {
        fetchSeasonGroupsTable: seasonApi.fetchSeasonGroupsTableCreator(dispatch),
        fetchSeasonLeagueTable: seasonApi.fetchSeasonLeagueTableCreator(dispatch),
        fetchSeasonMatches: seasonApi.fetchSeasonMatchesCreator(dispatch),
        fetchSeasonTeams: seasonApi.fetchSeasonTeamsCreator(dispatch),

        fetchTeam: teamApi.fetchTeamCreator(dispatch),
        fetchTeamMatches: teamApi.fetchTeamMatchesCreator(dispatch),
        fetchTeamPlayers: teamApi.fetchTeamPlayersCreator(dispatch)
    }
}