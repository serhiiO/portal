﻿import { fetchAction } from "../../actions/fetchAction.ts";

export function fetchSeasonGroupsTableCreator(dispatch: Redux.Dispatch) {
    return (seasonId: number, matchday?: number): Promise<FPortal.Model.Entities.FootballDataApi.GroupTableApiModel> => {
        const query = matchday ? `?matchday=${matchday}` : "";
        const requestUrl = `/api/soccerseasons/${seasonId}/groupTable${query}`;

        return fetchAction(dispatch, requestUrl)
            .then(response => response.json())
    }
}

export function fetchSeasonLeagueTableCreator(dispatch: Redux.Dispatch) {
    return (seasonId: number, matchday?: number): Promise<FPortal.Model.Entities.FootballDataApi.LeagueTableApiModel> => {
        const query = matchday ? `?matchday=${matchday}` : "";
        const requestUrl = `/api/soccerseasons/${seasonId}/leagueTable${query}`;

        return fetchAction(dispatch, requestUrl)
            .then(response => response.json());
    }
}

export function fetchSeasonMatchesCreator(dispatch: Redux.Dispatch) {
    return (seasonId: number, matchday?: number, timeFrame?: string): Promise<FPortal.Model.Entities.FootballDataApi.SeasonMatchesApiModel> => {
        let params: Array<string> = [];
        if (matchday)
            params.push(`matchday=${matchday}`);
        if (timeFrame)
            params.push(`timeFrame=${timeFrame}`);

        const query = params.length > 0 ? `?${params.join("&")}` : "";
        const requestUrl = `/api/soccerseasons/${seasonId}/fixtures${query}`;

        return fetchAction(dispatch, requestUrl)
            .then(response => response.json());
    }
}

export function fetchSeasonTeamsCreator(dispatch: Redux.Dispatch) {
    return (seasonId: number): Promise<FPortal.Model.Entities.FootballDataApi.SeasonTeamsApiModel> => {
        const requestUrl = `/api/soccerseasons/${seasonId}/teams`;
        return fetchAction(dispatch, requestUrl)
            .then(response => response.json());
    }
}