﻿import { fetchAction } from "../../actions/fetchAction.ts";

export function fetchTeamCreator(dispatch: Redux.Dispatch) {
    return (teamId: number): Promise<FPortal.Model.Entities.FootballDataApi.TeamApiModel> => {
        const requestUrl = `/api/teams/${teamId}`;

        return fetchAction(dispatch, requestUrl)
            .then(response => response.json())
    }
}

export function fetchTeamMatchesCreator(dispatch: Redux.Dispatch) {
    return (teamId: number, year?: number, timeFrame?: string, venue?: FPortal.Model.Entities.FootballDataApi.MatchVenue): Promise<FPortal.Model.Entities.FootballDataApi.TeamMatchesCollection> => {
        let params: Array<string> = [];
        if (year) params.push(`year=${year}`);
        if (timeFrame) params.push(`timeFrame=${timeFrame}`);
        if (venue) params.push(`venue=${venue}`);

        const query = params.length > 0 ? `?${params.join("&")}` : "";
        const requestUrl = `/api/teams/${teamId}/fixtures${query}`;

        return fetchAction(dispatch, requestUrl)
            .then(response => response.json())
    }
}

export function fetchTeamPlayersCreator(dispatch: Redux.Dispatch) {
    return (teamId: number): Promise<FPortal.Model.Entities.FootballDataApi.TeamPlayersApiModel> => {
        const requestUrl = `/api/teams/${teamId}/players`;

        return fetchAction(dispatch, requestUrl)
            .then(response => response.json())
    }
}