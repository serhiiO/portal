﻿import { FETCH_PENDING_SUFFIX, FETCH_FULFILLED_SUFFIX, FETCH_REJECTED_SUFFIX } from "../actions/actionTypes";

export const INCREMENT_LOADINGBAR = "loading-bar/INCREMENT";
export const DECREMENT_LOADINGBAR = "loading-bar/DECREMENT";

function incrementLoadbar(): Redux.IAction {
    return {
        type: INCREMENT_LOADINGBAR
    }
}

function decrementLoadbar(): Redux.IAction {
    return {
        type: DECREMENT_LOADINGBAR
    }
}

function loadbarMiddleware(): Redux.Middleware {
    return ({ dispatch, getState }) => next => action => {
        const result = next(action);

        if (action.type === undefined) {
            return result;
        }

        if (action.type.indexOf(FETCH_PENDING_SUFFIX) !== -1) {
            dispatch(incrementLoadbar());
        } else if (action.type.indexOf(FETCH_FULFILLED_SUFFIX) !== -1 || action.type.indexOf(FETCH_REJECTED_SUFFIX) !== -1) {
            dispatch(decrementLoadbar());
        }

        return result;
    }
}

export default loadbarMiddleware();