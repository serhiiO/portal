﻿import { createStore, applyMiddleware} from "redux";
import thunkMiddleware from "redux-thunk";
import loadbarMiddleware from "../middlewares/loadbarMiddleware.ts";
import rootReducer from "../reducers/rootReducer.ts";
import initState from "./initialState.ts";

export default function configurateStore(): Redux.Store {
    const createStoreWithMiddleware = applyMiddleware(thunkMiddleware, loadbarMiddleware)(createStore); 
    const store = createStoreWithMiddleware(rootReducer, initState(),
        window.devToolsExtension ? window.devToolsExtension() : undefined
    );
    return store;
}