﻿export default function initState(): FPortal.AppState {
    return {
        userName: null,
        loadbarState: 0
    };
}