﻿import {
    DEFAULT_FETCH,
    FETCH_FULFILLED_SUFFIX,
    FETCH_REJECTED_SUFFIX,
    FETCH_PENDING_SUFFIX
} from "./actionTypes.ts";



export function fetchPendingAction(actionPrefix: string = DEFAULT_FETCH): Redux.IAction {
    return {
        type: actionPrefix + FETCH_PENDING_SUFFIX
    };
}

export function fetchFullfilledAction(response: any, actionPrefix: string = DEFAULT_FETCH): Redux.IAction {
    return {
        type: actionPrefix + FETCH_FULFILLED_SUFFIX,
        payload: response
    };
}

export function fetchRejectedAction(reason: any, actionPrefix: string = DEFAULT_FETCH): Redux.IAction {
    return {
        type: actionPrefix + FETCH_REJECTED_SUFFIX,
        payload: reason
    };
}

export function createFetchAction(dispatch) {
    return (requestUrl: string | Request, init?: RequestInit, actionName: string = DEFAULT_FETCH): Promise<Response> => {
        dispatch(fetchPendingAction(actionName));
        return fetch(requestUrl, init)
            .then((response) => {
                dispatch(fetchFullfilledAction(response, actionName));
                return response;
            })
            .catch((reason) => {
                dispatch(fetchRejectedAction(reason, actionName));
                return reason;
            });
    }
}

export function fetchAction(dispatch: Redux.Dispatch, requestUrl: string | Request, init?: RequestInit, actionName: string = DEFAULT_FETCH): Promise<Response> {
    dispatch(fetchPendingAction(actionName));
    return fetch(requestUrl, init)
        .then((response) => {
            dispatch(fetchFullfilledAction(response, actionName));
            return response;
        })
        .catch((reason) => {
            dispatch(fetchRejectedAction(reason, actionName));
            return reason;
        });
}