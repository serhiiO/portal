﻿import { LOGIN_USER } from "./actionTypes";

export function loginUserAction(userName: string): Redux.IAction {
    return {
        type: LOGIN_USER,
        payload: userName
    };
}