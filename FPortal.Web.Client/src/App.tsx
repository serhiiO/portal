import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, browserHistory } from "react-router";
import { syncHistoryWithStore } from "react-router-redux";

import configurateStore from "./store/configuration.ts";
import Layout from "./layouts/DefaultLayout.tsx";
import Home from "./pages/Home.tsx";
import NotFound from "./pages/NotFound.tsx";
import Overview from "./pages/Overview.tsx";
import Team from "./pages/Team.tsx";

const store = configurateStore();
// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={Layout}>
                <IndexRoute component={Home} />
                <Route path="/overview/:key" component={Overview} />
                <Route path="/teams/:id" component={Team} />
                <Route path="*" component={NotFound} />
            </Route>
        </Router>
    </Provider>
    ,
    document.getElementById('redux-app')
);
