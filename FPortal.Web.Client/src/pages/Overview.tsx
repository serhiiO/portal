﻿import * as React from "react";
import { connect } from "react-redux";
import createSuiteApi from "../businessLogic/suiteApiClient.ts";
import competitionsLeagueMap from "../businessLogic/competitionsLeagueMap.ts";
import GroupStandings from "../components/GroupStandings.tsx";
import SeasonMatches from "../components/SeasonMatches.tsx";
import LeagueTable from "../components/LeagueTable.tsx";
import NotFoundPage from "./NotFound.tsx";

function mapActionsToPops(dispatch: Redux.Dispatch, ownProps?: AppProps.Pages.InternationalOwnProps): AppProps.Pages.InternationalConnectedProps {
    return {
        api: createSuiteApi(dispatch)
    };
}

class InternationalPage extends React.Component<AppProps.Pages.InternationalProps, AppStates.Pages.IInternationalState> {
    private tabs = [
        "Matches",
        "Standings"
    ]

    constructor(props: AppProps.Pages.InternationalProps, context: any) {
        super(props, context);
        this.state = {
            competition: undefined,
            activeTab: this.tabs[0]
        };
    }

    render() {
        if (this.state.competition === null)
            return <NotFoundPage />;

        if (this.state.competition === undefined)
            return null;

        let activeTabElement = null;
        if (this.state.activeTab == "Matches") {
            activeTabElement = <SeasonMatches seasonKey={this.state.competition.leagues[0]} />;
            
        }
        else if (this.state.activeTab == "Standings") {
            if (this.state.competition.international)
                activeTabElement = <GroupStandings seasonKey={this.state.competition.leagues[0]} />;
            else
                activeTabElement = <LeagueTable leagueKey={this.state.competition.leagues[0]} />
        }

        return (
            <div className="inner-container">
                <div className="page-title">{this.state.competition.name}</div>
                { this.createTabs() }
                { activeTabElement }
            </div>
        );
    }

    // React Element builders
    createTabs(): JSX.Element {
        const tabs = this.tabs.map((tab) => {
            return <li key={tab} className={"tab" + (this.state.activeTab == tab ? " active" : "") } onClick={ e => this.setState({ activeTab: tab }) }>{tab}</li>
        })

        return (
            <div className="nav-tabs-container">
                <div className="flexslider-tab">
                    <ul className="nav-tabs">
                        {tabs}
                    </ul>
                </div>
            </div>
        );
    }

    // React Lifecycle Methods
    componentWillReceiveProps(nextProps: AppProps.Pages.InternationalOwnProps, nextContext: any): void {
        if (nextProps.params.key != this.props.params.key) {
            this.updateCompetition(nextProps.params.key);
        }
    }

    componentDidMount(): void {
        this.updateCompetition(this.props.params.key);
    }

    // Logic
    updateCompetition(competiotionKey: string) {
        const competition = _.find(competitionsLeagueMap, (element) => element.key == competiotionKey);
        if (competition)
            this.setState({ competition: competition, activeTab: this.tabs[0] });
        else
            this.setState({ competition: null, activeTab: this.tabs[0] });
    }
}

export default connect(null, mapActionsToPops)(InternationalPage);