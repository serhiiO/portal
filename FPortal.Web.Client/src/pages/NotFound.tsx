﻿import * as React from "react";

export default class NotFoundPage extends React.Component<ReactRouter.RouteComponentProps<any, any>, any> {
    render() {
        return (
            <div className="not-found-container">
                <div className="inner-container">
                    <h1>404</h1>
                    <div className="not-found-text">NOT FOUND</div>
                    <div className="underline"></div>
                    <div className="message">
                        <span>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.Aenean consequat tortor fermentum mi
                            fermentum dignissim.Nullam vel ipsum ut ligula elementum lobortis.
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}