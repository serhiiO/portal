import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

export default class HomePage extends React.Component<any, {}> {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return <h1>Home page</h1>;
    }
}