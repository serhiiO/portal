﻿import * as React from "react";
import { connect } from "react-redux";
import createSuiteApi from "../businessLogic/suiteApiClient.ts";
import NotFoundPage from "./NotFound.tsx";

function mapActionsToPops(dispatch: Redux.Dispatch, ownProps?: AppProps.Pages.TeamOwnProps): AppProps.Pages.TeamConnectedProps {
    return {
        api: createSuiteApi(dispatch)
    };
}

class TeamPage extends React.Component<AppProps.Pages.TeamProps, AppStates.Pages.TeamProps> {
    private playersPositions = {
        goalkeepers: [
            "Keeper"
        ],
        defenders: [
            "Left-Back",
            "Right-Back",
            "Centre Back"
        ],
        midfielders: [
            "Defensive Midfield",
            "Central Midfield",
            "Attacking Midfield"
        ],
        forwards: [
            "Secondary Striker",
            "Left Wing",
            "Right Wing",
            "Centre Forward",
        ]
    }

    constructor(props: AppProps.Pages.TeamProps, context: any) {
        super(props, context);
        this.state = {
            team: undefined,
            teamMatches: undefined,
            teamPlayers: undefined
        };
    }

    render() {
        const { team, teamPlayers, teamMatches } = this.state;

        if (team == undefined || teamPlayers == undefined)
            return null;
        if (team == null || teamPlayers == null)
            return <NotFoundPage />;

        return (
            <div className="inner-container">
                <div className="team">
                    <div className="team-info">
                        <img src={team.crestUrl} width="300px" height="300px" />
                        <h3>{team.name}</h3>
                        <div><span>Short name: </span><span>{team.shortName || team.code}</span></div>
                        <div><span>Squad market value: </span><span>{team.squadMarketValue}</span></div>
                    </div>
                    <div className="team-players">
                        { this.createPlayersTable(this.state.teamPlayers.players) }
                    </div>
                </div>
            </div>
        );
    }

    // React Element builders
    createPlayersTable = (players: FPortal.Model.Entities.FootballDataApi.TeamPlayer[]) => {
        const goalkeepers = players.filter(player => player.position == this.playersPositions.goalkeepers[0]);
        const defenders = [].concat.apply([], this.playersPositions.defenders.map(position => players.filter(player => player.position == position)));
        const midfielders = [].concat.apply([], this.playersPositions.midfielders.map(position => players.filter(player => player.position == position)));
        const forwards = [].concat.apply([], this.playersPositions.forwards.map(position => players.filter(player => player.position == position)));

        return (
            <table>
                <tbody>
                    <tr>Goalkeepers</tr>
                    { goalkeepers.map(this.createPlayerRow) }
                    <tr>Defenders</tr>
                    { defenders.map(this.createPlayerRow) }
                    <tr>Midfielders</tr>
                    { midfielders.map(this.createPlayerRow) }
                    <tr>Forwards</tr>
                    { forwards.map(this.createPlayerRow) }
                </tbody>
            </table>
        );
    }

    createPlayerRow = (player: FPortal.Model.Entities.FootballDataApi.TeamPlayer): JSX.Element => {
        return (
            <tr key={player.jerseyNumber} >
                <td>{player.jerseyNumber}</td>
                <td>{player.name}</td>
                <td>{player.position}</td>
                <td>{player.nationality}</td>
                <td>{player.dateOfBirth}</td>
                <td>{player.contractUntil}</td>
                <td>{player.marketValue}</td>
            </tr>
        );
    }

    // React Lifecycle Methods
    componentDidMount(): void {
        this.loadTeamData(this.props.params.id);
    }

    componentWillReceiveProps(nextProps: AppProps.Pages.TeamProps, nextContext: any): void {
        if (nextProps.params.id != this.props.params.id) {
            this.loadTeamData(nextProps.params.id);
        }
    }

    // Logic
    loadTeamData = (teamId: number) => {
        this.props.api.fetchTeam(teamId)
            .then(value => this.setState({ team: value }))
            .catch(error => this.setState({ team: null }));

        this.props.api.fetchTeamMatches(teamId)
            .then(value => this.setState({ teamMatches: value }))
            .catch(error => this.setState({ teamMatches: null }));

        this.props.api.fetchTeamPlayers(teamId)
            .then(value => this.setState({ teamPlayers: value }))
            .catch(error => this.setState({ teamPlayers: null }));
    }
}

export default connect(null, mapActionsToPops)(TeamPage);