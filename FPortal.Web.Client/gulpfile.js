'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var del = require('del');
var path = require('path');
var merge = require('merge-stream');
var runSequence = require('run-sequence');
var webpack = require('webpack');
var argv = require('minimist')(process.argv.slice(2));

// Settings
var DEST = '../FPortal.Web.Server';             // The build output folder
var PUBLIC_PATH = './';                         // Public path for webpack
var APP_PATH = "/app";
var CONTENT_PATH = "/content";
var RELEASE = !!argv.release;                 // Minimize and optimize during a build?
var AUTOPREFIXER_BROWSERS = [                 // https://github.com/ai/autoprefixer
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

var src = {};
var watch = false;
var pkgs = (function () {
    var temp = {};
    var map = function (source) {
        for (var key in source) {
            temp[key.replace(/[^a-z0-9]/gi, '')] = source[key].substring(1);
        }
    };
    map(require('./package.json').dependencies);
    return temp;
}());

// The default task
gulp.task('default', ['build']);

// Clean up
gulp.task('clean', del.bind(null, [DEST + APP_PATH, DEST + CONTENT_PATH], { force: true }));

// Static files
gulp.task('assets', function () {
    src.assets = 'src/assets/**';
    return gulp.src(src.assets)
      .pipe($.changed(DEST))
      .pipe(gulp.dest(DEST))
      .pipe($.size({ title: 'assets' }));
});

// Images
gulp.task('images', function () {
    src.images = 'src/images/**';
    return gulp.src(src.images)
      .pipe($.changed(DEST + CONTENT_PATH + '/images'))
      .pipe($.cache($.imagemin({
          progressive: true,
          interlaced: true
      })))
      .pipe(gulp.dest(DEST + CONTENT_PATH + '/images'))
      .pipe($.size({ title: 'images' }));
});

// Static files
gulp.task('fonts', function () {
    src.fonts = 'src/fonts/**';
    return gulp.src(src.fonts)
      .pipe($.changed(DEST + CONTENT_PATH + '/fonts'))
      .pipe(gulp.dest(DEST + CONTENT_PATH + '/fonts'))
      .pipe($.size({ title: 'fonts' }));
});

// HTML views
gulp.task('views', function () {
    src.views = 'views/**/*.html';
    return gulp.src(src.views)
      .pipe($.changed(DEST))
      .pipe($.if(RELEASE, $.htmlmin({
          removeComments: true,
          collapseWhitespace: true,
          minifyJS: true
      })))
      .pipe(gulp.dest(DEST))
      .pipe($.size({ title: 'views' }));
});

// CSS style sheets
gulp.task('styles', function () {
    src.styles = 'src/styles/**/*.{css,less}';
    return gulp.src('src/styles/bootstrap.less')
      .pipe($.plumber())
      .pipe($.less({
          sourceMap: !RELEASE,
          sourceMapBasepath: __dirname
      }))
      .on('error', console.error.bind(console))
      .pipe($.autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }))
      .pipe($.csscomb())
      .pipe($.if(RELEASE, $.minifyCss()))
      .pipe(gulp.dest(DEST + CONTENT_PATH + '/css'))
      .pipe($.size({ title: 'styles' }));
});

// Bundle
gulp.task('bundle', function (cb) {
    var started = false;
    var config = require('./config/webpack.js')(RELEASE, DEST + APP_PATH, PUBLIC_PATH + APP_PATH);
    var bundler = webpack(config);

    function bundle(err, stats) {
        if (err) {
            throw new $.util.PluginError('webpack', err);
        }

        $.util.log('[webpack]', stats.toString({ colors: true }));

        if (!started) {
            started = true;
            return cb();
        }
    }

    if (watch) {
        bundler.watch(200, bundle);
    } else {
        bundler.run(bundle);
    }
});

// Build the app from source code
gulp.task('build', ['clean'], function (cb) {
    runSequence(['images', 'fonts', 'styles', 'bundle'], cb);
});

gulp.task('build-watch', function (cb) {

    watch = true;

    runSequence('build', function () {
        gulp.watch(src.assets, ['assets']);
        gulp.watch(src.images, ['images']);
        gulp.watch(src.fonts, ['fonts']);
        gulp.watch(src.views, ['views']);
        gulp.watch(src.styles, ['styles']);
        cb();
    });
});
