﻿using FPortal.Model.Entities.Common;
using Ploeh.AutoFixture.Xunit;
using Xunit;
using Xunit.Extensions;

namespace FPortal.Model.UnitTest
{
    public class UrlQueryBuilderTests
    {
        [Theory, AutoData]
        public void BuildQueryWithOneParameter(string key, object value)
        {
            var expected = $"?{key}={value.ToString()}";

            var query = new UrlQueryBuilder();
            query.Add(key, value);
            var actual = query.ToString();

            Assert.Equal(expected, actual);
        }

        [Theory, AutoData]
        public void BuildQueryWithTwoParameter(string key1, object value1, string key2, object value2)
        {
            var expected = $"?{key1}={value1.ToString()}&{key2}={value2.ToString()}";

            var query = new UrlQueryBuilder();
            query.Add(key1, value1);
            query.Add(key2, value2);
            var actual = query.ToString();

            Assert.Equal(expected, actual);
        }

        [Theory, AutoData]
        public void BuildQueryWithStructParameter(string key, int value)
        {
            var expected = $"?{key}={value.ToString()}";

            var query = new UrlQueryBuilder();
            query.Add(key, value);
            var actual = query.ToString();

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineAutoData("random", default(object))]
        public void BuildEmptyQuery(string key, object value)
        {
            var query = new UrlQueryBuilder();
            query.Add(key, value);
            var actual = query.ToString();

            Assert.Equal(string.Empty, actual);
        }

        [Theory]
        [InlineAutoData("random", default(int))]
        public void BuildEmptyQueryWithStructParameter(string key, int value)
        {
            var query = new UrlQueryBuilder();
            query.Add(key, value);
            var actual = query.ToString();

            Assert.Equal(string.Empty, actual);
        }
    }
}
