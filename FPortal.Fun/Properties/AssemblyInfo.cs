using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("FPortal.Fun")]
[assembly: AssemblyDescription("Functional library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("24-7 Entertainment")]
[assembly: AssemblyProduct("FPortal.Fun")]
[assembly: AssemblyCopyright("Copyright 24-7 Entertainment 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: NeutralResourcesLanguage("en")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
