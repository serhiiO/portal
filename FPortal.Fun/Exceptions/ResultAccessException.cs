﻿using System;

namespace FPortal.Fun.Exceptions
{
    public sealed class ResultAccessException : Exception
    {
        public ResultAccessException(string message) : base(message){}
    }
}
