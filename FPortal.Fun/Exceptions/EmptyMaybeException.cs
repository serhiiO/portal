﻿using System;

namespace FPortal.Fun.Exceptions
{
    public sealed class EmptyMaybeException : Exception
    {
        const string message = 
            "Attempted to access the value of a Maybe<T> when it was empty, " +
            "you must ALWAYS check for a value before attempting to access it.";

        public EmptyMaybeException() : base(message){}

        public EmptyMaybeException(Exception innerException) 
            : base(message, innerException){}
    }
}
