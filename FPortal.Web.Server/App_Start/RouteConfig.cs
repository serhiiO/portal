﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FPortal.Web.Server
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("root", "", new { controller = "ReduxApp", action = "Index" });
            routes.MapRoute("redux-app", "{*catchall}", new { controller = "ReduxApp", action = "Index" });

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("api/{*pathInfo}");
            routes.IgnoreRoute("bundles/{*pathInfo}");

            routes.MapMvcAttributeRoutes();
        }
    }
}
