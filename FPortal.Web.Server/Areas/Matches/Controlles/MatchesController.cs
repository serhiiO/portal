﻿using FPortal.Model.Entities.FootballDataApi;
using FPortal.Model.Services.FootballDataApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace FPortal.Web.Server.Areas.Matches.Controlles
{
    public class MatchesController : ApiController
    {
        IMatchesService MatchesService { get; }

        public MatchesController(IMatchesService matchesService)
        {
            if (matchesService == null)
                throw new ArgumentNullException(nameof(matchesService));

            MatchesService = matchesService;
        }

        //[CacheOutput(ClientTimeSpan = 1200)]
        [Route("api/fixtures/")]
        public async Task<IHttpActionResult> GetMatchesDetails(string timeFrame = default(string), LeagueCode league = default(LeagueCode))
        {
            var result = await MatchesService.GetMatchesDetailsCollection(TimeFrame.FromString(timeFrame), league);
            return Ok(result);
        }

        [Route("api/fixtures/{id}")]
        public async Task<IHttpActionResult> GetMatch(int id, int head2head = default(int))
        {
            var result = await MatchesService.GetMatch(id, head2head);
            return Ok(result);
        }
    }
}