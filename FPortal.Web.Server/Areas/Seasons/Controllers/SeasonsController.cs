﻿using FPortal.Model.Entities.FootballDataApi;
using FPortal.Model.Services.FootballDataApi;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace FPortal.Web.Server.Areas.Seasons.Controllers
{
    public class SeasonsController : ApiController
    {
        ISeasonService SeasonService { get; }

        public SeasonsController(ISeasonService seasonService)
        {
            if (seasonService == null)
                throw new ArgumentNullException(nameof(seasonService));

            SeasonService = seasonService;
        }

        //[CacheOutput(ClientTimeSpan = 1200)]
        [Route("api/soccerseasons/")]
        public async Task<IHttpActionResult> GetSeasons(int year = default(int))
        {
            return Ok(await SeasonService.GetAllAvailableSeasons(year));
        }

        [Route("api/soccerseasons/{id}/leagueTable")]
        public async Task<IHttpActionResult> GetSeasonLeagueTable(int id, int matchday = default(int))
        {
            return Ok(await SeasonService.GetLeagueTable(id, matchday));
        }

        [Route("api/soccerseasons/{id}/groupTable")]
        public async Task<IHttpActionResult> GetSeasonGroupTable(int id, int matchday = default(int))
        {
            return Ok(await SeasonService.GetGroupTable(id, matchday));
        }

        [Route("api/soccerseasons/{id}/fixtures")]
        public async Task<IHttpActionResult> GetSeasonMatches(int id, int matchday = default(int), string timeFrame = default(string))
        {
            return Ok(await SeasonService.GetMatches(id, matchday, TimeFrame.FromString(timeFrame)));
        }

        [Route("api/soccerseasons/{id}/teams")]
        public async Task<IHttpActionResult> GetSeasonTeams(int id)
        {
            return Ok(await SeasonService.GetTeams(id));
        }
    }
}
