﻿using FPortal.Model.Entities.FootballDataApi;
using FPortal.Model.Services.FootballDataApi;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace FPortal.Web.Server.Areas.Teams.Controllers
{
    public class TeamController : ApiController
    {
        ITeamService TeamService { get; }

        public TeamController(ITeamService teamService)
        {
            if (teamService == null)
                throw new ArgumentNullException(nameof(teamService));

            TeamService = teamService;
        }

        [Route("api/teams/{id}")]
        public async Task<IHttpActionResult> GetTeam(int id)
        {
            var result = await TeamService.GetTeam(id);
            return Ok(result);
        }

        [Route("api/teams/{id}/fixtures")]
        public async Task<IHttpActionResult> GetTeamsMatches(int id, int year = default(int), string timeFrame = default(string), MatchVenue venue = default(MatchVenue))
        {
            var result = await TeamService.GetTeamMatches(id, year, TimeFrame.FromString(timeFrame), venue);
            return Ok(result);
        }

        [Route("api/teams/{id}/players")]
        public async Task<IHttpActionResult> GetTeamsPlayers(int id)
        {
            var result = await TeamService.GetTeamPlayers(id);
            return Ok(result);
        }
    }
}