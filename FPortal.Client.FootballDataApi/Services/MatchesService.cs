﻿using FPortal.Model.Services.FootballDataApi;
using System.Threading.Tasks;
using FPortal.Model.Entities.FootballDataApi;
using FPortal.Fun;
using System.Net.Http;

namespace FPortal.Client.FootballDataApi.Services
{
    sealed class MatchesService : BaseService, IMatchesService
    {
        public async Task<MatchDetailsCollection> GetMatchesDetailsCollection(TimeFrame timeFrame = default(TimeFrame), LeagueCode league = default(LeagueCode))
        {
            return await ApiCollection.GetMatchesDetails(league, timeFrame)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<MatchDetailsCollection>);
        }

        public async Task<Match> GetMatch(int id, int head2head = default(int))
        {
            return await ApiCollection.GetMatch(id, head2head)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<Match>);
        }
    }
}
