﻿using FPortal.Fun;
using FPortal.Model.Entities.FootballDataApi;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace FPortal.Client.FootballDataApi.Services
{
    abstract class BaseService
    {
        public BaseService() { }

        private HttpClient CreateHttpClient()
        {
            var client = new HttpClient()
            {
                BaseAddress = new Uri(ApiCollection.Domain)
            };

            foreach (var header in ApiCollection.DefaultHeaders)
                client.DefaultRequestHeaders.Add(header.Key, header.Value);

            return client;
        }

        protected virtual async Task<TResponse> SendAsync<TResponse>(HttpRequestMessage requestMessage)
        {
            using (var client = CreateHttpClient())
            {
                return await requestMessage
                    .MapAsync(client.SendAsync)
                    .MapAsync(ParseResponse<TResponse>);
            }
        }

        protected virtual async Task<TResult> ParseResponse<TResult>(HttpResponseMessage responseMessage)
        {
            var content = await responseMessage.EnsureSuccessStatusCode()
                .MapAsync(x => x.Content.ReadAsStringAsync());
            var result = JsonConvert.DeserializeObject<TResult>(content);
            return result;
        }
    }
}
