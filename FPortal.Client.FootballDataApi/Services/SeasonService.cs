﻿using FPortal.Model.Services.FootballDataApi;
using System.Collections.Generic;
using System.Threading.Tasks;
using FPortal.Model.Entities.FootballDataApi;
using System.Net.Http;
using FPortal.Fun;
using System.Linq;

namespace FPortal.Client.FootballDataApi.Services
{
    sealed class SeasonService : BaseService, ISeasonService
    {
        public async Task<IEnumerable<FootballSeasonApiModel>> GetAllAvailableSeasons(int year = default(int))
        {
            return await ApiCollection.GetAvailableSeasons(year)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<IEnumerable<FootballSeasonApiModel>>)
                .MapAsync(AddMissingSeasons);
        }

        private async Task<IEnumerable<FootballSeasonApiModel>> AddMissingSeasons(IEnumerable<FootballSeasonApiModel> seasons)
        {
            //Euro 2016
            var euro2016Task = ApiCollection.GetLeagueTable(424)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<GroupTableApiModel>)
                .Map(result => new FootballSeasonApiModel()
                {
                    Id = 424,
                    Caption = result.LeagueCaption,
                    CurrentMatchday = result.Matchday,
                    League = LeagueCode.EU16
                });

            var missing = await Task.WhenAll(euro2016Task);
            return seasons.Concat(missing);
        }

        public async Task<GroupTableApiModel> GetGroupTable(int id, int matchday = default(int))
        {
            return await ApiCollection.GetLeagueTable(id, matchday)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<GroupTableApiModel>);
        }

        public async Task<LeagueTableApiModel> GetLeagueTable(int id, int matchday = default(int))
        {
            return await ApiCollection.GetLeagueTable(id, matchday)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<LeagueTableApiModel>);
        }

        public async Task<SeasonMatchesApiModel> GetMatches(int id, int matchday = default(int), TimeFrame timeFrame = default(TimeFrame))
        {
            var result = await ApiCollection.GetSeasonMatchesDetails(id, matchday, timeFrame)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<SeasonMatchesApiModel>);

            return result;
        }

        public async Task<SeasonTeamsApiModel> GetTeams(int id)
        {
            return await id
                .Map(ApiCollection.GetTeamsInSeason)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<SeasonTeamsApiModel>);
        }
    }
}
