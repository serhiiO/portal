﻿using System.Threading.Tasks;
using FPortal.Model.Entities.FootballDataApi;
using FPortal.Model.Services.FootballDataApi;
using FPortal.Fun;
using System.Net.Http;
using System;

namespace FPortal.Client.FootballDataApi.Services
{
    sealed class TeamService : BaseService, ITeamService
    {
        public async Task<TeamApiModel> GetTeam(int id)
        {
            return await ApiCollection.GetTeam(id)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<TeamApiModel>);
        }

        public async Task<TeamMatchesCollection> GetTeamMatches(int id, int year = default(int), TimeFrame timeFrame = default(TimeFrame), MatchVenue venue = default(MatchVenue))
        {
            return await ApiCollection.GetTeamMatches(id, year, timeFrame, venue)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<TeamMatchesCollection>);
        }

        public async Task<TeamPlayersApiModel> GetTeamPlayers(int id)
        {
            return await ApiCollection.GetTeamPlayers(id)
                .Map(url => new HttpRequestMessage(HttpMethod.Get, url))
                .MapAsync(SendAsync<TeamPlayersApiModel>);
        }
    }
}
