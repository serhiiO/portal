﻿using FPortal.Client.FootballDataApi.Services;
using FPortal.Model.Services.FootballDataApi;
using Ninject.Modules;

namespace FPortal.Client.FootballDataApi.Infrastructure.IoC
{
    public class FootballDataApiModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ISeasonService>().To<SeasonService>();
            Kernel.Bind<IMatchesService>().To<MatchesService>();
            Kernel.Bind<ITeamService>().To<TeamService>();
        }
    }
}
