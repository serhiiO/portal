﻿using FPortal.Model.Entities.FootballDataApi;
using System.Threading.Tasks;

namespace FPortal.Model.Services.FootballDataApi
{
    public interface ITeamService
    {
        Task<TeamApiModel> GetTeam(int id);
        Task<TeamMatchesCollection> GetTeamMatches(int id, int year = default(int), TimeFrame timeFrame = default(TimeFrame), MatchVenue venue = default(MatchVenue));
        Task<TeamPlayersApiModel> GetTeamPlayers(int id);
    }
}
