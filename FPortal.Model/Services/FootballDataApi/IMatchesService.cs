﻿using FPortal.Model.Entities.FootballDataApi;
using System.Threading.Tasks;

namespace FPortal.Model.Services.FootballDataApi
{
    public interface IMatchesService
    {
        Task<MatchDetailsCollection> GetMatchesDetailsCollection(TimeFrame timeFrame = default(TimeFrame), LeagueCode league = default(LeagueCode));
        Task<Match> GetMatch(int id, int head2head = default(int));
    }
}