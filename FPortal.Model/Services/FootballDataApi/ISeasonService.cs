﻿using FPortal.Model.Entities.FootballDataApi;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FPortal.Model.Services.FootballDataApi
{
    public interface ISeasonService
    {
        Task<IEnumerable<FootballSeasonApiModel>> GetAllAvailableSeasons(int year = default(int));
        Task<GroupTableApiModel> GetGroupTable(int id, int matchday = default(int));
        Task<LeagueTableApiModel> GetLeagueTable(int id, int matchday = default(int));
        Task<SeasonMatchesApiModel> GetMatches(int id, int matchday = default(int), TimeFrame timeFrame = default(TimeFrame));
        Task<SeasonTeamsApiModel> GetTeams(int id);
    }
}
