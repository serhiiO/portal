﻿using System.Collections.Generic;
using System.Linq;

namespace FPortal.Model.Entities.Common
{
    public class UrlQueryBuilder
    {
        private Dictionary<string, string> QueryParams;

        public UrlQueryBuilder(params object[] queryParams)
        {
            QueryParams = new Dictionary<string, string>();
        }

        public void Add<T>(string key, T value)
        {
            if (EqualityComparer<T>.Default.Equals(value, default(T)))
                return;

            QueryParams.Add(key, value.ToString());
        }

        public override string ToString()
        {
            return QueryParams.Any() ? $"?{string.Join("&", QueryParams.Select(x => x.Key + "=" + x.Value).ToArray())}" : string.Empty;
        }
    }
}
