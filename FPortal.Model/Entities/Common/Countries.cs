﻿using TypeLite;

namespace FPortal.Model.Entities.Common
{
    [TsEnum]
    public enum Countries
    {
        None = 0,
        Ukraine,
        Russia,
        England,
        Spain,
        Germany,
        Italy,
        France,
        Netherlands,
        Portugal
    }
}
