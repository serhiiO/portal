﻿using Newtonsoft.Json;
using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class Match
    {
        [JsonProperty("fixture")]
        public MatchDetailsApiModel MatchDetails { get; set; }

        [JsonProperty("head2head")]
        public HeadToHead HeadToHead { get; set; }
    }
}
