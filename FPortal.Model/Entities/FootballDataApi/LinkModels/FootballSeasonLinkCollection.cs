﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class FootballSeasonLinkCollection : LinkCollection
    {
        public Link Fixtures { get; set; }
        public Link LeagueTable { get; set; }
        public Link Teams { get; set; }
    }
}
