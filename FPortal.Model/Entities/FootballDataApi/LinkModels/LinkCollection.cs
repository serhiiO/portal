﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class LinkCollection
    {
        public Link Self { get; set; }
    }
}
