﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class TeamLinkCollection : LinkCollection
    {
        public Link Fixtures { get; set; }
        public Link Players { get; set; }
    }
}
