﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class MatchDetailsLinkCollection : LinkCollection
    {
        public Link Soccerseason { get; set; }

        public Link HomeTeam { get; set; }

        public Link AwayTeam { get; set; }
    }
}
