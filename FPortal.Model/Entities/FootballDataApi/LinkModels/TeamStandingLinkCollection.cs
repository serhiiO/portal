﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class TeamStandingLinkCollection
    {
        public Link Team { get; set; }
    }
}
