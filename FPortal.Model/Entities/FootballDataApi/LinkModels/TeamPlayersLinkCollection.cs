﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class TeamPlayersLinkCollection : LinkCollection
    {
        public Link Team { get; set; }
    }
}
