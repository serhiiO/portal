﻿using TypeLite;
using System;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class Link
    {
        private Uri _href;

        public Uri Href
        {
            get
            {
                return _href;
            }

            set
            {
                _href = value.IsAbsoluteUri ?
                     new Uri(value.PathAndQuery.Replace(ApiCollection.ApiVersion.ToString(), "api"), UriKind.Relative) :
                     value;
            }
        }
    }
}
