﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class TeamMatchesLinkCollection : LinkCollection
    {
        public Link Team { get; set; }
    }
}
