﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class SeasonMatchesLinkCollection : LinkCollection
    {
        public Link Soccerseason { get; set; }
    }
}
