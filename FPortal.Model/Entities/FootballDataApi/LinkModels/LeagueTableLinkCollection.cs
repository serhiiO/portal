﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class LeagueTableLinkCollection : LinkCollection
    {
        public Link Soccerseason { get; set; }
    }
}
