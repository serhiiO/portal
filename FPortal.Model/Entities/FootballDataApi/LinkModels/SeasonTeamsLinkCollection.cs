﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi.LinkModels
{
    [TsClass]
    public class SeasonTeamsLinkCollection : LinkCollection
    {
        public Link Soccerseason { get; set; }
    }
}
