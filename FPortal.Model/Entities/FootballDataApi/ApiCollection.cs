﻿using FPortal.Model.Entities.Common;
using System.Collections.Generic;
using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsEnum]
    public enum ApiVersion
    {
        v1,
        alpha
    }

    public static class ApiCollection
    {
        public static ApiVersion ApiVersion { get; set; }

        public static string Domain { get { return "http://api.football-data.org/"; } }

        public static Dictionary<string, string> DefaultHeaders
        {
            get
            {
                var result = new Dictionary<string, string>();
                result.Add("X-Auth-Token", "97f0a5217e174ff5b8862525725b076d");

                return result;
            }
        }

        static ApiCollection()
        {
            ApiVersion = ApiVersion.v1;
        }

        /// <summary>
        /// List all available soccer seasons.
        /// </summary>
        /// <param name="season">specific seasons year (default = current season)</param>
        /// <returns></returns>
        public static string GetAvailableSeasons(int season = default(int))
        {
            var query = new UrlQueryBuilder();
            query.Add("season", season);
            return $"/{ApiVersion.ToString()}/soccerseasons" + query.ToString();
        }

        /// <summary>
        /// List all teams for a certain soccerseason.
        /// </summary>
        /// <param name="id">season id</param>
        /// <returns></returns>
        public static string GetTeamsInSeason(int id)
        {
            return $"/{ApiVersion.ToString()}/soccerseasons/{id}/teams";
        }

        /// <summary>
        /// Show League Table / current standing.
        /// </summary>
        /// <param name="id">league id</param>
        /// <param name="matchDay">specific matchday (default = current matchday)</param>
        /// <returns></returns>
        public static string GetLeagueTable(int id, int matchDay = default(int))
        {
            var query = new UrlQueryBuilder();
            query.Add("matchday", matchDay);
            return $"/{ApiVersion.ToString()}/soccerseasons/{id}/leagueTable" + query.ToString();
        }

        /// <summary>
        /// List all fixtures for a certain soccerseason.
        /// </summary>
        /// <param name="id">season id</param>
        /// <param name="matchDay">specific matchday (default = all fixtures for this season)</param>
        /// <param name="timeFrame">past or next days limit (default = none)</param>
        /// <returns></returns>
        public static string GetSeasonMatchesDetails(int id, int matchDay = default(int), TimeFrame timeFrame = default(TimeFrame))
        {
            var query = new UrlQueryBuilder();
            query.Add("matchday", matchDay);
            query.Add("timeFrame", timeFrame);
            return $"/{ApiVersion.ToString()}/soccerseasons/{id}/fixtures" + query.ToString();
        }

        /// <summary>
        /// List fixtures across a set of soccerseasons.
        /// </summary>
        /// <param name="leagues">list of involved leagues (default = all available leagues)</param>
        /// <param name="timeFrame">past or next days limit (default = next 7 days)</param>
        /// <returns></returns>
        public static string GetMatchesDetails(LeagueCode leagues = default(LeagueCode) /** TODO: switch to typed **/, TimeFrame timeFrame = default(TimeFrame))
        {
            var query = new UrlQueryBuilder();
            query.Add("league", leagues);
            query.Add("timeFrame", timeFrame);
            return $"/{ApiVersion.ToString()}/fixtures/" + query.ToString();
        }

        /// <summary>
        /// Show one fixture.
        /// </summary>
        /// <param name="id">fixture id</param>
        /// <param name="head2head">define the the number of former games to be analyzed in the head2head node (default = 10)</param>
        /// <returns></returns>
        public static string GetMatch(int id, int head2head = default(int))
        {
            var query = new UrlQueryBuilder();
            query.Add("head2head", head2head);

            return $"/{ApiVersion.ToString()}/fixtures/{id}" + query.ToString();
        }


        /// <summary>
        /// Show all fixtures for a certain team.
        /// </summary>
        /// <param name="id">team id</param>
        /// <param name="season">specific season year (default = current seasons)</param>
        /// <param name="timeFrame">past or next days limit (default = next 7 days)</param>
        /// <param name="venue">defines the venue of a fixture (default = unset and means to return all fixtures)</param>
        /// <returns></returns>
        public static string GetTeamMatches(int id, int season = default(int), TimeFrame timeFrame = default(TimeFrame), MatchVenue venue = default(MatchVenue))
        {
            var query = new UrlQueryBuilder();
            query.Add("season", season);
            query.Add("timeFrame", timeFrame);
            query.Add("venue", venue);

            return $"/{ApiVersion.ToString()}/teams/{id}/fixtures" + query.ToString();
        }

        /// <summary>
        /// Show one team.
        /// </summary>
        /// <param name="id">team id</param>
        /// <returns></returns>
        public static string GetTeam(int id)
        {
            return $"/{ApiVersion.ToString()}/teams/{id}";
        }

        /// <summary>
        /// Show all players for a certain team.
        /// </summary>
        /// <param name="id">team id</param>
        public static string GetTeamPlayers(int id)
        {
            return $"/{ApiVersion.ToString()}/teams/{id}/players";
        }
    }
}
