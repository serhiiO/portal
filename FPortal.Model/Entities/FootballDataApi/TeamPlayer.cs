﻿using TypeLite;
using System;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class TeamPlayer
    {
        public string Name { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Nationality { get; set; }

        public string Position { get; set; }

        public string MarketValue { get; set; }

        public string JerseyNumber { get; set; }

        public DateTime ContractUntil { get; set; }
    }
}
