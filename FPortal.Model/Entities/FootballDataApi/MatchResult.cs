﻿using Newtonsoft.Json;
using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class MatchResult
    {
        [JsonProperty("goalsAwayTeam", NullValueHandling = NullValueHandling.Ignore)]
        public uint? GoalsAwayTeam { get; set; }

        [JsonProperty("goalsHomeTeam", NullValueHandling = NullValueHandling.Ignore)]
        public uint? GoalsHomeTeam { get; set; }
    }
}
