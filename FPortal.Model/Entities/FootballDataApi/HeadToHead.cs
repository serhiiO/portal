﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class HeadToHead : MatchDetailsCollection
    {
        public int HomeTeamWins { get; set; }

        public int AwayTeamWins { get; set; }

        public MatchDetailsApiModel LastWinHomeTeam { get; set; }

        public MatchDetailsApiModel LastWinAwayTeam { get; set; }
    }
}
