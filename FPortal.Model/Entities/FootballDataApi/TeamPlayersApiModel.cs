﻿using FPortal.Model.Entities.FootballDataApi.LinkModels;
using TypeLite;
using System.Collections.Generic;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class TeamPlayersApiModel : BaseApiModel<TeamPlayersLinkCollection>
    {
        public int Count { get; set; }

        public IEnumerable<TeamPlayer> Players { get; set; }
    }
}
