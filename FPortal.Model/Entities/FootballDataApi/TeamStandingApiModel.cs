﻿using FPortal.Model.Entities.FootballDataApi.LinkModels;
using TypeLite;
using System;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class TeamStandingApiModel : BaseApiModel<TeamStandingLinkCollection>
    {
        public string TeamName { get; set; }

        public Uri CrestURI { get; set; }

        public uint Position { get; set; }

        public uint Points { get; set; }

        public uint PlayedGames { get; set; }

        public uint Wins { get; set; }

        public uint Draws { get; set; }

        public uint Losses { get; set; }

        public uint Goals { get; set; }

        public uint GoalsAgainst { get; set; }

        public int GoalDifference { get; set; }

        public TeamPlayStatistic Home { get; set; }

        public TeamPlayStatistic Away { get; set; }
    }
}
