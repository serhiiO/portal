﻿using System;

namespace FPortal.Model.Entities.FootballDataApi
{
    public class GroupTeamStandingApiModel
    {
        public string Group { get; set; }

        public uint TeamId { get; set; }

        public string Team { get; set; }

        public Uri CrestURI { get; set; }

        public uint Rank { get; set; }

        public uint Points { get; set; }

        public uint PlayedGames { get; set; }

        public uint Goals { get; set; }

        public uint GoalsAgainst { get; set; }

        public int GoalDifference { get; set; }
    }
}
