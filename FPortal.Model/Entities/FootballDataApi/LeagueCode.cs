﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsEnum]
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LeagueCode
    {
        EMPTY,
        BL1, //german 1
        BL2, // german 2
        BL3, // german 3
        FL1, // france 1
        FL2, // france 2
        PL, // england 1 ?
        PD, // spain 1
        SD, // spain 2
        SA, // italia 1
        PPL, // ??
        DED, // netherlands ?
        CL, // champions league
        EL1, // ??
        EU16
    }
}
