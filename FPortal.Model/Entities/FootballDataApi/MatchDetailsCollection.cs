﻿using Newtonsoft.Json;
using TypeLite;
using System;
using System.Collections.Generic;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class MatchDetailsCollection
    {
        [JsonProperty("fixtures")]
        public IEnumerable<MatchDetailsApiModel> MatchesDetails { get; set; }

        public int Count { get; set; }

        public DateTime TimeFrameStart { get; set; }

        public DateTime TimeFrameEnd { get; set; }
    }
}
