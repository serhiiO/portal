﻿using Newtonsoft.Json;
using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class BaseApiModel<TLinkCollection>
    {
        [JsonProperty("_links")]
        [TsProperty(Name = "_links")]
        public TLinkCollection Links { get; set; }
    }
}
