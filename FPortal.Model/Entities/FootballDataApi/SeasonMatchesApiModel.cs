﻿using FPortal.Model.Entities.FootballDataApi.LinkModels;
using Newtonsoft.Json;
using TypeLite;
using System.Collections.Generic;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class SeasonMatchesApiModel : BaseApiModel<SeasonMatchesLinkCollection>
    {
        public IEnumerable<MatchDetailsApiModel> Fixtures { get; set; }

        public uint Count { get; set; }
    }
}
