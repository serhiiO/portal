﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class GroupTableApiModel
    {
        public string LeagueCaption { get; set; }

        [JsonProperty("matchday", NullValueHandling = NullValueHandling.Ignore)]
        public uint Matchday { get; set; }

        public IDictionary<string, IEnumerable<GroupTeamStandingApiModel>> Standings { get; set; }
    }
}
