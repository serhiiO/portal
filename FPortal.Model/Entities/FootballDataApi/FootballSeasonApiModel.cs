﻿using FPortal.Model.Entities.FootballDataApi.LinkModels;
using Newtonsoft.Json;
using TypeLite;
using System;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class FootballSeasonApiModel : BaseApiModel<FootballSeasonLinkCollection>
    {
        public int Id { get; set; }

        public string Caption { get; set; }

        [JsonProperty("currentMatchday", NullValueHandling = NullValueHandling.Ignore)]
        public uint CurrentMatchday { get; set; }

        public DateTime LastUpdated { get; set; }

        public LeagueCode League { get; set; }

        public uint NumberOfGames { get; set; }

        public uint NumberOfMatchdays { get; set; }

        public uint NumberOfTeams { get; set; }

        public uint Year { get; set; }
    }
}
