﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class TeamPlayStatistic
    {
        public uint Wins { get; set; }

        public uint Draws { get; set; }

        public int Losses { get; set; }

        public uint Goals { get; set; }

        public uint GoalsAgainst { get; set; }
    }
}
