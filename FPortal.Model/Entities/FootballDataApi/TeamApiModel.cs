﻿using FPortal.Model.Entities.FootballDataApi.LinkModels;
using TypeLite;
using System;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class TeamApiModel : BaseApiModel<TeamLinkCollection>
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }

        public Uri CrestUrl { get; set; }

        public string SquadMarketValue { get; set; }
    }
}
