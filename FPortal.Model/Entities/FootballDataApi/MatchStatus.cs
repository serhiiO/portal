﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsEnum]
    public enum MatchStatus
    {
        NONE,
        TIMED,
        IN_PLAY,
        FINISHED,
        SCHEDULED
    }
}
