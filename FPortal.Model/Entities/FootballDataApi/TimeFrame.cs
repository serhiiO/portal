﻿using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class TimeFrame
    {
        private bool Next;
        private int Days;

        public TimeFrame(bool next, int days)
        {
            Next = next;
            Days = days;
        }

        public override string ToString()
        {
            var prefix = Next ? "n" : "p";
            return prefix + Days;
        }

        public static TimeFrame FromString(string str)
        {
            if (string.IsNullOrEmpty(str))
                return default(TimeFrame);

            try
            {
                var next = str.StartsWith("n");
                str = str.Replace("n", string.Empty);
                str = str.Replace("p", string.Empty);
                var days = int.Parse(str);
                return new TimeFrame(next, days);
            }
            catch
            {
                return default(TimeFrame);
            }
        }
    }
}
