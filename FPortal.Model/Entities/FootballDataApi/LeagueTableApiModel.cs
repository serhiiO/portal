﻿using FPortal.Model.Entities.FootballDataApi.LinkModels;
using Newtonsoft.Json;
using TypeLite;
using System.Collections.Generic;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class LeagueTableApiModel : BaseApiModel<LeagueTableLinkCollection>
    {
        public string LeagueCaption { get; set; }

        [JsonProperty("matchday", NullValueHandling = NullValueHandling.Ignore)]
        public uint Matchday { get; set; }

        public IEnumerable<TeamStandingApiModel> Standing { get; set; }
    }
}
