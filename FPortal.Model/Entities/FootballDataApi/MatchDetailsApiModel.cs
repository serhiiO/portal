﻿using FPortal.Model.Entities.FootballDataApi.LinkModels;
using TypeLite;
using System;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class MatchDetailsApiModel : BaseApiModel<MatchDetailsLinkCollection>
    {
        public DateTime Date { get; set; }

        public string HomeTeamName { get; set; }

        public string AwayTeamName { get; set; }

        public uint Matchday { get; set; }

        public MatchStatus Status { get; set; }

        public MatchResult Result { get; set; }
    }
}
