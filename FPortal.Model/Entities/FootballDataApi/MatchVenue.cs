﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{

    [TsEnum]
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MatchVenue
    {
        unset,
        home,
        away
    }
}
