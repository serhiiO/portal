﻿using FPortal.Model.Entities.FootballDataApi.LinkModels;
using System.Collections.Generic;
using TypeLite;

namespace FPortal.Model.Entities.FootballDataApi
{
    [TsClass]
    public class SeasonTeamsApiModel : BaseApiModel<SeasonTeamsLinkCollection>
    {
        public IEnumerable<TeamApiModel> Teams { get; set; }

        public uint Count { get; set; }
    }
}
